package com.uniquindio.electiva_android.curremtfilmsactivity.util;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Esta clase es la encargada de proveer el contenido de la base de datos de esta aplicacion
 * a toda app que quiera obtener la
 *
 * @author Einer
 * @version 1.0 6/05/2015
 */
public class FilmsProvider extends ContentProvider {

    //uri por medio de la cual se accedera a la informacion compartida
    private static final String uri =
            "content://com.uniquindio.electiva_android.curremtfilmsactivity/films";
    public static final Uri CONTENT_URI = Uri.parse(uri);

    //Necesario para UriMatcher
    private static final int FILMS = 1;
    private static final int FILMS_ID = 2;
    private static final UriMatcher uriMatcher;

    //Base de datos
    private FilmSQLiteHelper filmdbh;

    //Inicializamos el UriMatcher para las dos estructuras de acceso
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("com.uniquindio.electiva_android.curremtfilmsactivity", "films", FILMS);
        uriMatcher.addURI("com.uniquindio.electiva_android.curremtfilmsactivity", "films/#", FILMS_ID);
    }

    /**
     * metodo constructor de la clase
     */
    public FilmsProvider() {
    }

    /**
     * metodo encargado de inicializar la base de datos
     */
    @Override
    public boolean onCreate() {

        filmdbh = new FilmSQLiteHelper(getContext(), Utilities.NAME_DB, null, Utilities.VERSION_DB);
        return true;
    }


    /**
     * metodo encargado de realizar el select en la base de datos
     *
     * @param uri           uri que indica que tipo de peticion se desea realizar
     * @param projection    nombre de las colunnas que se desean devolver
     * @param selection     Condición de la query
     * @param selectionArgs Argumentos variables de la query
     * @param sortOrder     Orden de los resultados
     * @return cursor con el resultado de la peticion
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        if (uriMatcher.match(uri) == FILMS_ID) {
            where = "ID=" + uri.getLastPathSegment();
        }

        SQLiteDatabase db = filmdbh.getWritableDatabase();

        Cursor c = db.query(Utilities.NAME_DB, projection, where,
                selectionArgs, null, null, sortOrder);

        return c;


    }

    /**
     * metodo encargado de realizar una insersion en la bd desde otra app
     *
     * @param uri    uri que indica que tipo de peticion se desea realizar
     * @param values valores que se debean agregar en la base de datos
     * @return deveulve el uri del elemento agregado
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        long regId = 1;

        SQLiteDatabase db = filmdbh.getWritableDatabase();

        regId = db.insert(Utilities.NAME_DB, null, values);


        Uri newUri = ContentUris.withAppendedId(CONTENT_URI, regId);

        return newUri;

    }

    /**
     * actualiza la base de datos desde otra app
     *
     * @param uri           uri que indica que tipo de peticion se desea realizar
     * @param values        valores que se debean agregar en la base de datos
     * @param selection
     * @param selectionArgs
     * @return
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int cont;

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        if (uriMatcher.match(uri) == FILMS_ID) {
            where = "ID=" + uri.getLastPathSegment();
        }

        SQLiteDatabase db = filmdbh.getWritableDatabase();

        cont = db.update(Utilities.NAME_DB, values, where, selectionArgs);

        return cont;

    }

    /**
     * metodo encargado de eliminar un elemento en la base de datos
     *
     * @param uri           uri que indica que tipo de peticion se desea realizar
     * @param selection     condicional de la consulta
     * @param selectionArgs argumentos del where
     * @return numero del elemento eliminado en la db
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        int cont;

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        if (uriMatcher.match(uri) == FILMS_ID) {
            where = "ID=" + uri.getLastPathSegment();
        }

        SQLiteDatabase db = filmdbh.getWritableDatabase();

        cont = db.delete(Utilities.NAME_DB, where, selectionArgs);

        return cont;

    }

    /**
     * obtener el tipo de peticion que se quiere hacer
     *
     * @param uri uri que indica que tipo de peticion se desea realizar
     * @return devuelve el matcher por medio del cual se hizo la peticion
     */
    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);

        switch (match) {
            case FILMS:
                return "vnd.android.cursor.dir/vnd.electiva_android.film";
            case FILMS_ID:
                return "vnd.android.cursor.item/vnd.electiva_android.film";
            default:
                return null;
        }
    }
}
