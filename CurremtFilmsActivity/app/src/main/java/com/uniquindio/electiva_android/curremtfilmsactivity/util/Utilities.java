package com.uniquindio.electiva_android.curremtfilmsactivity.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.uniquindio.electiva_android.curremtfilmsactivity.CurrentFilmsActivity;
import com.uniquindio.electiva_android.curremtfilmsactivity.R;
import com.uniquindio.electiva_android.curremtfilmsactivity.fragment.AddFilmFragment;
import com.uniquindio.electiva_android.curremtfilmsactivity.vo.Film;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * Esta clase provee de utilidades a la aplicación
 *
 * @author Einer
 * @version 1.0 28/04/2015
 */
public class Utilities {

    public static String URL_SERVER = "http://films-einergauch0.rhcloud.com/film";

    public static String GET_LIST_FILMS = "1";
    public static String GET_FILMS = "2";
    public static String INSERT_FILMS = "3";
    public static String DELETE_FILMS = "4";
    public static String UPDATE_FILMS = "5";

    public static int REQUEST_CODE_CURRENT_FILM = 1;
    public static int CALLED_INTERNAL_CURRENT_FILM = 2;
    public final static String CALLED_INTERNAL = "calledInternal";
    public final static String NAME_FILM = "nameFilm";
    public final static String NAME_DB = "FilmDB";
    public final static int VERSION_DB = 1;
    public final static String MY_PREFERENCES = "MisPreferencias";
    public final static String LANGUAGE_PREFERENCES = "languaje_preferences";
    public final static String FILM = "film";
    public final static String LANGUAGE_ES = "es";
    public final static String LANGUAGE_EN = "en";

    /**
     * este método es encargado de mostrar un mensaje en pantalla
     *
     * @param context contexto de la actividad que invoca el método
     * @param message mensaje a mostrar al usuario
     */
    public static void showAlert(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * este método es el encargado de mostrar el dialogo por medio del cual se van a agregar peliculas
     *
     * @param fragmentManager permite realizar la trasaccion del dialogo
     * @param nameClass       nombre de la actividad que lo invoco
     */
    public static void showDialogAddFilm(FragmentManager fragmentManager, String nameClass) {
        AddFilmFragment dialogaAdFilm = new AddFilmFragment();
        dialogaAdFilm.setStyle(dialogaAdFilm.STYLE_NORMAL, R.style.MyAddDialog);
        dialogaAdFilm.show(fragmentManager, nameClass);
    }

    /**
     * metodo encargado de cambiar el lenguaje de la aplicaicion
     *
     * @param context contexto de la actividad que invoca el método
     */
    public static void changeLanguage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFERENCES, context.MODE_PRIVATE);
        String language = prefs.getString(LANGUAGE_PREFERENCES, LANGUAGE_EN);
        if (language.equals(LANGUAGE_ES)) {
            language = LANGUAGE_EN;
        } else if (language.equals(LANGUAGE_EN)) {
            language = LANGUAGE_ES;
        }
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LANGUAGE_PREFERENCES, language);
        editor.commit();
        //getLanguage(context);
    }

    /**
     * método encargado de obtener y cambiar el lengueje con el que se quiere mostrar la aplicacion
     *
     * @param context contexto de la aplicacion
     */
    public static void getLanguage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFERENCES, context.MODE_PRIVATE);
        String language = prefs.getString(LANGUAGE_PREFERENCES, LANGUAGE_EN);
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getApplicationContext().getResources().updateConfiguration(config, null);
    }

    /**
     * este metodo dice si el dispositivo esta conectado a internet
     *
     * @return devuelve si cuando esta connectado y falso cuando no
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected()) ? true : false;
    }

    /**
     * Este método transforma un JSON en una pelicula
     *
     * @param jsonFilm JSON con formato de la película
     * @return la pelucula con la informacion del JSON
     */
    public static Film converJSONToFilm(String jsonFilm) {

        Gson gson = new Gson();
        Film film = gson.fromJson(jsonFilm, Film.class);
        Log.d("CurrentFilmsActivity", "prelicula res " + film.getTitle());

        return film;
    }

    /**
     * Este método tranforma una pelicula a un String con formato JSON
     *
     * @param film película que se desea transformar
     * @return string con la info de la pelicula con el formato de JSON
     */
    public static String converFilmToJSON(Film film) {

        Gson gson = new Gson();
        String json = gson.toJson(film);

        return json;
    }

    /**
     * Este método permite obtener el KeyHash para agregar en facebook
     */
    public static void getKeyHash(Context context) {

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo("com.uniquindio.electiva_android.curremtfilmsactivity", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(CurrentFilmsActivity.class.getSimpleName(), "1 KeyHash Error: "+e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.d(CurrentFilmsActivity.class.getSimpleName(), "2 KeyHash Error: "+e.getMessage());
        }

    }

}
