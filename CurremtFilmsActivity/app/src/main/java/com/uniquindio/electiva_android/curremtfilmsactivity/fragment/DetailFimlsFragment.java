package com.uniquindio.electiva_android.curremtfilmsactivity.fragment;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.uniquindio.electiva_android.curremtfilmsactivity.R;
import com.uniquindio.electiva_android.curremtfilmsactivity.util.Utilities;
import com.uniquindio.electiva_android.curremtfilmsactivity.vo.Film;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Este fragmento contiene el detalle de cada uno de las peliculas seleccionadas en la lista
 *
 * @author Einer Zapata
 * @version 1.0 on 25/03/2015.
 */
public class DetailFimlsFragment extends Fragment implements View.OnClickListener {

    @InjectView(R.id.film_detail_title)
    protected TextView title;

    @InjectView(R.id.film_detail_description)
    protected TextView description;

    @InjectView(R.id.go_to_trailer)
    protected Button goToTrailer;

    @InjectView(R.id.delete_film)
    protected Button btnDelete;

    @InjectView(R.id.btn_login_button)
    protected LoginButton btnLoginFacebook;

    @InjectView(R.id.btn_share_on_face)
    protected Button btnShareOnFace;

    private UiLifecycleHelper uiHelper;
    private FilmDetailListener filmDetailListener;
    private Film film;

    /**
     * método inicial del ciclo de vida del fragmento
     *
     * @param activity actividad padre del fragmento
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            filmDetailListener = (FilmDetailListener) activity;
        } catch (ClassCastException e) {
            Log.v("ListFimlsFragment", "error en el casting");
        }
    }

    /**
     * Es este método se inicializa el uiHelper para manejar la sesion de facebook
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // To maintain FB Login session
        uiHelper = new UiLifecycleHelper(getActivity(), statusCallback);
        uiHelper.onCreate(savedInstanceState);
    }

    /**
     * contiene el view que representa el fragment
     *
     * @param inflater           inflador para adaptar el layout
     * @param container          contenenor donde se agregará el layout
     * @param savedInstanceState información que con tiene extras
     * @return el view adaptado al tamaño del contenedor
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail_fimls, container, false);
        ButterKnife.inject(this, rootView);

        btnLoginFacebook.setFragment(this);
        btnLoginFacebook.setEnabled(true);
        btnLoginFacebook.setReadPermissions(Arrays.asList("email", "public_profile"));
        btnLoginFacebook.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                if (user != null) {
                    Log.d(DetailFimlsFragment.class.getSimpleName(), "conectado en facebook 2 " + user);
                }
            }
        });

        return rootView;
    }

    /**
     * Se encarga de manejar los estados de la sesion de facebook
     */
    private Session.StatusCallback statusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    /**
     * Es invocado desde el statusCallback, en esta se administran los cambios de la sesion en facebook
     *
     * @param session   objeto con las caracteristicas de la sesion
     * @param state     estado de la session
     * @param exception excepción de la sesion
     */
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {

        if (state.isOpened()) {
            btnLoginFacebook.setVisibility(View.INVISIBLE);
            btnShareOnFace.setVisibility(View.VISIBLE);
        } else if (state.isClosed()) {
            Log.d(DetailFimlsFragment.class.getSimpleName(), "desconectado de facebook");
        }
    }

//    /**
//     * @param savedInstanceState
//     */
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//    }

    /**
     * cambia la informacion contenida en los controles por la de la pelicula seleccionada
     *
     * @param film pelicula de la que se desea mostrar la informacion detallada
     */
    public void showDetail(Film film) {
        this.film = film;
        Log.d(DetailFimlsFragment.class.getSimpleName(), "este es el titulo: " + film.getId());
        title.setText(film.getTitle());
        description.setText(film.getDescription());
    }

    /**
     * metodó callback escucha el evento del boton goToTrailer
     *
     * @param v view que gestiono el evento
     */
    @OnClick({R.id.delete_film, R.id.go_to_trailer, R.id.btn_share_on_face})
    public void onClick(View v) {

        if (v.getId() == goToTrailer.getId()) {

            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(film.getUrlTrailer()));
                startActivityForResult(intent, 0);

            } catch (ActivityNotFoundException e) {
                Log.e("Error en la url", e.getMessage());
            }

        }

        if (v.getId() == btnDelete.getId()) {
            if (film.getId() != null) {
                filmDetailListener.onDeleteFilmChoose(film.getId());
            } else {
                filmDetailListener.onDeleteFilmChoose(film.getTitle());
            }
        }

        if (v.getId() == btnShareOnFace.getId()) {

            Session session = Session.getActiveSession();
            //SessionState state = SessionState.OPENED;

            if (session != null && session.isOpened()) {
                FacebookDialog shareDialog =
                        new FacebookDialog.ShareDialogBuilder(
                                getActivity())
                                .setName(getString(R.string.show_the_film) + ": " + film.getTitle())
                                .setLink(film.getUrlTrailer())
                                .build();
                uiHelper.trackPendingDialogCall(shareDialog.present());
            } else {
                Utilities.showAlert(getActivity(), getString(R.string.you_need_to_connect));
            }

        }

    }

    /**
     * método en el que entra cuando vuelve del trailern
     * Se administran las respuestas de facebook cuando se hace loguin y se comparte
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {

            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {

                boolean didComplete = FacebookDialog.getNativeDialogDidComplete(data);

                if (didComplete) {
                    //Get Native Dialog Completion Gesture
                    String nativeDialogCompletionGesture = FacebookDialog.getNativeDialogCompletionGesture(data);

                    if (nativeDialogCompletionGesture == null || FacebookDialog.COMPLETION_GESTURE_CANCEL.equals(nativeDialogCompletionGesture)) {
                        //Show Publish Cancel Toast
                    } else {
                        //Show Success Post Toast
                    }
                } else {
                    //Show Publish Cancel Toast
                }


            }
        });

    }

    /**
     * interfaz por medio de la cual se activa el callback de la actividad padre
     * encargado de eliminar una pelicula por nombre
     */
    public interface FilmDetailListener {
        void onDeleteFilmChoose(String nameFilm);
    }

    /**
     * Borra los controles y eventos inyectados por medio del Butter Knife
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    /**
     * este método es usado para menejar el ciclo de vida de la sesion de facebook
     */
    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    /**
     * este método es usado para menejar el ciclo de vida de la sesion de facebook
     */
    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    /**
     * este método es usado para menejar el ciclo de vida de la sesion de facebook
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    /**
     * este método es usado para menejar el ciclo de vida de la sesion de facebook
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }


}
