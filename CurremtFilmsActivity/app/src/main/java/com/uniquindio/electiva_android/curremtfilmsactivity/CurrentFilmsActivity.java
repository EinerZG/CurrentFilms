package com.uniquindio.electiva_android.curremtfilmsactivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.uniquindio.electiva_android.curremtfilmsactivity.fragment.AddFilmFragment;
import com.uniquindio.electiva_android.curremtfilmsactivity.fragment.DetailFimlsFragment;
import com.uniquindio.electiva_android.curremtfilmsactivity.fragment.ListFimlsFragment;
import com.uniquindio.electiva_android.curremtfilmsactivity.util.FilmSQLiteHelper;
import com.uniquindio.electiva_android.curremtfilmsactivity.util.Utilities;
import com.uniquindio.electiva_android.curremtfilmsactivity.vo.Film;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Actividad principal de la aplicación
 * Permite la navegacion entre las peliculas
 *
 * @author Einer Zapata
 * @version 1.0 on 25/03/2015.
 */
public class CurrentFilmsActivity extends ActionBarActivity
        implements ListFimlsFragment.FilmListener, SearchView.OnQueryTextListener, AddFilmFragment.ListenerAddFilms, DetailFimlsFragment.FilmDetailListener {

    public static ArrayList<Film> films;
    private SearchView mSearchView;
    private ShareActionProvider mShareActionProvider;
    private boolean isFragment;
    protected ListFimlsFragment listFimlsFragment;
    private FilmSQLiteHelper usdbh;
    private SQLiteDatabase db;

    /**
     * metodo callback donde se deben realizar las funciones pesadas
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utilities.getLanguage(this);
        setContentView(R.layout.activity_current_films);
        ButterKnife.inject(this);

        films = new ArrayList<Film>();
        usdbh =
                new FilmSQLiteHelper(this, Utilities.NAME_DB, null, 1);
        db = usdbh.getWritableDatabase();
        getInfoDB();

        init();

        Utilities.getKeyHash(this);

    }

    /**
     * inicializa los controles y framentos de la actividad
     */
    public void init() {

        listFimlsFragment = (ListFimlsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_films_list);

        isFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_films_details) != null;

        if (isFragment) {
            //onFilmChoose(1);
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        //actionBar.hide();//Ocultar ActionBar
        actionBar.setIcon(R.mipmap.ic_launcher);             //Establecer icono
        actionBar.setTitle(getString(R.string.app_name));        //Establecer titulo

        if (films.size() > 0) {
            actionBar.setSubtitle(getString(R.string.app_recommended) + ": " + films.get(0).getTitle());     //Establecer Subtitulo     }
        }

        updateListFilmServer();

    }

    /**
     * método callback donde se inicializan el menu y se obtiene instacias de los controles
     * cargados en el actionbar
     *
     * @param menu menu que se mostrará en el actionbar
     * @return verdadero
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_current_films, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setOnQueryTextListener(this);

        MenuItem shareItem = menu.findItem(R.id.share);
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(getDefaultShareIntent());

        return true;
    }

    /**
     * método callback que permite identificar con que elemento del actionbar esta interactuando el usuario
     *
     * @param item con lo que interactua el usuario
     * @return el metodo de la super clase
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            updateListFilmServer();
            return true;
        }

        if (id == R.id.language) {
            Utilities.changeLanguage(this);
            Intent intent = new Intent(this, CurrentFilmsActivity.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_add) {
            Utilities.showDialogAddFilm(getSupportFragmentManager(), CurrentFilmsActivity.class.getSimpleName());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void updateListFilmServer() {

        if (!Utilities.isConnected(this)) {
            Utilities.showAlert(this, getString(R.string.is_connected));
        } else {
            SecondThread secondThread = new SecondThread(this);
            secondThread.execute(Utilities.GET_LIST_FILMS);
        }

    }

    /**
     * método calback que es llamado cuando se presiona un item de la lista de peliculas
     *
     * @param position del item presionado
     */
    @Override
    public void onFilmChoose(int position) {

        Film filmChoose = films.get(position - 1);

        if (isFragment) {
            if (films.size() > 0) {
                ((DetailFimlsFragment)
                        getSupportFragmentManager().findFragmentById(R.id.fragment_films_details)).showDetail(filmChoose);
            }
        } else {
            Intent intent = new Intent(CurrentFilmsActivity.this, DetailFilmActivity.class);
            intent.putExtra(Utilities.FILM, filmChoose);
            startActivityForResult(intent, Utilities.REQUEST_CODE_CURRENT_FILM);
        }
    }

    /**
     * este método recibe informacion desde la actividad llamada con anterioridad
     *
     * @param requestCode codigo enviado
     * @param resultCode  numero de respuesta
     * @param data        informacion envidad desde la otra atividad
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utilities.REQUEST_CODE_CURRENT_FILM) {
            if (null != data) {
                deleteFilm(data.getStringExtra(Utilities.NAME_FILM));
            }
        }

    }

    /**
     * metodo callback que se activa cada que se da submit al control de SearchView
     *
     * @param s texto que contiene el control
     * @return falso
     */
    @Override
    public boolean onQueryTextSubmit(String s) {

        return false;
    }

    /**
     * metodo callback que se activa cada que se cambia la cadena que contiene el control de SearchView
     *
     * @param s texto que contiene el control
     * @return falso
     */
    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    /**
     * metodo para compartir informacion por medio de  cualquier
     * actividad que proveea ese servicios
     *
     * @return la intecion que contiene la informacion a compartir
     */
    private Intent getDefaultShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "SUBJECT");
        intent.putExtra(Intent.EXTRA_TEXT, "Extra Text");
        return intent;
    }

    /**
     * este método permite agregar una nueva pelicula
     *
     * @param film pelicula que se desea agregar
     */
    @Override
    public void onAddFill(Film film) {

        String filmJson = Utilities.converFilmToJSON(film);
        SecondThread secondThread = new SecondThread(this);
        secondThread.execute(Utilities.INSERT_FILMS, filmJson);

        //insertFilm(film);
        //films.add(film);
        //listFimlsFragment.updateAdapter();
    }

    /**
     * Este método agrega una nueva pelicula a la base de datos
     *
     * @param film pelicula a ser agregada
     */
    private void insertFilm(Film film) {
        db.execSQL("INSERT INTO FilmDB (TITLE,YEAR,URLTRAILER,DESCRIPTION)\n" +
                "VALUES ( '" + film.getTitle() + "', '" + film.getYear() + "', '" + film.getUrlTrailer() + "', '" + film.getDescription() + "' )");
    }

    /**
     * este método permite obtener loe elementos agregados a la base de datos
     */
    public void getInfoDB() {
        String[] fields = new String[]{"TITLE", "YEAR", "URLTRAILER", "DESCRIPTION"};
        Cursor c = db.query("FilmDB", fields, null, null, null, null, null);

        if (c.moveToFirst()) {
            do {
                String title = c.getString(0);
                String year = c.getString(1);
                String url = c.getString(2);
                String description = c.getString(3);
                films.add(new Film(title, year, url, description));
                Log.d("CurrentFilmsActivity", "titulo: " + title + " año: " + year + " url: " + url + " descrición: " + description);
            } while (c.moveToNext());
        }
    }

    /**
     * este método eliminar una pelicula agregada a la base de datos por le nombre
     *
     * @param nameFilm nombre de la pelicula que se desea eliminar
     */
    public void deleteFilm(String nameFilm) {
        //db.execSQL("DELETE FROM FilmDB WHERE TITLE= '" + nameFilm + "'");
        //films.clear();
        //getInfoDB();
        //listFimlsFragment.updateAdapter();

        deleteFilmArray(nameFilm);
        listFimlsFragment.updateAdapter();
        SecondThread secondThread = new SecondThread(this);
        secondThread.execute(Utilities.DELETE_FILMS, nameFilm);

    }

    /**
     * metodo que dise cual es la pelicula que se desea leiminar
     *
     * @param nameFilm nombre de la pelicula a eliminar de la base de datos
     */
    @Override
    public void onDeleteFilmChoose(String nameFilm) {
        deleteFilm(nameFilm);
    }

    /**
     * Este método se encarga de obtener todas las películas en el servicio
     */
    public void getListFilmsServer() {

        HttpClient httpClient = new DefaultHttpClient();
        HttpGet request =
                new HttpGet(Utilities.URL_SERVER);
        request.setHeader("content-type", "application/json");

        try {

            HttpResponse resp = httpClient.execute(request);
            String respStr = EntityUtils.toString(resp.getEntity());
            Gson gson = new Gson();
            Type tipoListaFilms = new TypeToken<List<Film>>() {
            }.getType();
            List<Film> serverFilms = gson.fromJson(respStr, tipoListaFilms);

            if (serverFilms.size() > 0) {
                for (Film film : serverFilms) {
                    films.add(film);
                }
            }

        } catch (Exception e) {
            Log.e("ServicioRest", "Error! cargando lista de peliculas ", e);
        }

    }

    /**
     * Este método recibe una pelicula en formato JSON para agregar la en el servicio REST
     * @param jsonFilm con la estructura de la película
     */
    public void insertFilmServer(String jsonFilm) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post =
                new HttpPost(Utilities.URL_SERVER);
        post.setHeader("content-type", "application/json");

        try {

            StringEntity entity = new StringEntity(jsonFilm);
            post.setEntity(entity);

            HttpResponse respose = httpClient.execute(post);
            String resp = EntityUtils.toString(respose.getEntity());

            Film film = Utilities.converJSONToFilm(resp);
            films.add(film);

            Log.d(SecondThread.class.getSimpleName(), "repuesta de insert " + resp);

        } catch (Exception e) {
            Log.e("ServicioRest", "Error! insercion de película " + e);
        }

    }

    /**
     * Elimina un elemento de la lista por el id de la película
     * @param idFilm id de la pemicula a eliminar
     */
    public void deleteFilmArray(String idFilm) {

        for (Film f : films) {
            if (f.getId().equals(idFilm)) {
                films.remove(f);
                return;
            }
        }

    }

    /**
     * Elimina una película del servidor por id
     * @param idFilm id de la pemicula a eliminar
     */
    public void deleteFilmServer(String idFilm) {

        HttpClient client = new DefaultHttpClient();
        HttpDelete delete = new HttpDelete(Utilities.URL_SERVER + "/" + idFilm);
        delete.setHeader("content-type", "application/json");

        try {
            HttpResponse response = client.execute(delete);
            String res = EntityUtils.toString(response.getEntity());
            Log.d(CurrentFilmsActivity.class.getSimpleName(), "id a eliminar: " + res);
        } catch (Exception e) {
            Log.e("ServicioRest", "Error! eliminando la pelicula " + e);
        }

    }

    /**
     * Esta clase se encarga de manejar las tareas en segundo plano de la aplicacion
     * más precisamente las comunicaciones con el servicio
     */
    public class SecondThread extends AsyncTask<String, Integer, Integer> {

        private ProgressDialog progress;
        private Context context;

        /**
         * Constructor de la clase por medio del cual se inicializa el contexto
         * @param context contexto de la actividad que invoco la tarea
         */
        public SecondThread(Context context) {
            this.context = context;
        }

        /**
         * Es creado para inicializar el progress dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(context, getString(R.string.load_films),
                    getString(R.string.please_wait), true);
        }

        /**
         * Este metodo es el que se ivoca para realizar las tareas en segundo plano
         * @param params parametros de la tarea de realizar
         * @return
         */
        @Override
        protected Integer doInBackground(String... params) {

            if (params[0].equals(Utilities.GET_LIST_FILMS)) {
                getListFilmsServer();
            }
            else if (params[0].equals(Utilities.INSERT_FILMS)) {
                insertFilmServer(params[1]);
            }
            else if (params[0].equals(Utilities.DELETE_FILMS)) {
                deleteFilmServer(params[1]);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        /**
         * Con este método se actiliza la lista del adapter
         * y ocultar el cargador
         * @param integer
         */
        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            listFimlsFragment.updateAdapter();
            progress.dismiss();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onCancelled(Integer integer) {
            super.onCancelled(integer);
        }
    }

}